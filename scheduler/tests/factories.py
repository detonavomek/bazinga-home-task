import factory
import factory.fuzzy
from django.utils import timezone

from scheduler import models

class CustomerFactory(factory.Factory):
    class Meta:
        model = models.Customer

    name = factory.fuzzy.FuzzyText(length=16)
    interval = factory.fuzzy.FuzzyInteger(1, 4)
    start_datetime = factory.LazyFunction(timezone.now)


class TargetFactory(factory.Factory):
    class Meta:
        model = models.Target

    customer = factory.SubFactory(CustomerFactory)
    email = factory.Sequence(lambda n: f'target{n}@gmail.com')
    sending_datetime = factory.LazyFunction(timezone.now)


class TargetLogFactory(factory.Factory):
    class Meta:
        model = models.TargetLog

    target = factory.SubFactory(TargetFactory)
    email = factory.LazyAttribute(lambda a: a.target.email)
    scheduled_datetime = factory.LazyAttribute(lambda a: a.target.sending_datetime)
    sending_datetime = factory.LazyFunction(timezone.now)
    send = True
    error_message = None
