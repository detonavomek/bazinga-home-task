import random
from datetime import timedelta

from django.utils import timezone
from celery import shared_task

from .models import Target, TargetLog


def fake_email_send(email):
    # 9 of 10 emails sent successful
    if not random.randint(0, 10):
        return False, 'Fake Error Message'
    return True, None

@shared_task
def send_target_mails():
    # get targets for last hour
    time_start = timezone.now().replace(minute=0, second=0, microsecond=0)
    time_finish = time_start + timedelta(hours=1)
    targets = Target.objects.filter(
        sending_datetime__range=(time_start, time_finish))

    # send email for each target; save logs; define next sending time
    for target in targets:
        TargetLog.add_log(target, *fake_email_send(target.email))
        target.update_sending_datetime()
