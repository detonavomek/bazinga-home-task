from django.contrib import admin
from django.utils import timezone

from .models import Customer, Target, TargetLog


class TargetInlineAdmin(admin.TabularInline):
    model = Target
    fields = ('email',)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'interval', 'start_datetime')
    inlines = [TargetInlineAdmin,]

    def has_change_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False

    def save_formset(self, request, form, formset, change):
        [instance.save() for instance in formset.save(commit=False)]
        formset.save_m2m()
        form.instance.save()


class TargetAdmin(admin.ModelAdmin):
    list_display = ('email', 'get_customer', 'sending_datetime')

    def has_add_permission(self, request, obj=None):
        return False
    
    def has_change_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False

    def get_customer(self, obj):
        return obj.customer.name

    get_customer.short_description = 'Customer'
    get_customer.admin_order_field = 'customer__name'


class TargetLogAdmin(admin.ModelAdmin):
    list_display = ('email', 'get_target',
            'scheduled_datetime', 'sending_datetime', 'send', 'error_message')
    
    def has_add_permission(self, request, obj=None):
        return False
    
    def has_change_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False
    
    def get_target(self, obj):
        return f'{obj.target.customer.name}: {obj.target.email}'

    get_target.short_description = 'Target'
    get_target.admin_order_field = 'target__email'


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Target, TargetAdmin)
admin.site.register(TargetLog, TargetLogAdmin)
