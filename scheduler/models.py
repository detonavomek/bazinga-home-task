from datetime import timedelta

from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone

from .validators import validate_start_datetime


class Customer(models.Model):
    INTERVALS = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
    )
    name = models.CharField(max_length=255)
    interval = models.IntegerField(default=INTERVALS[0][0], choices=INTERVALS)
    start_datetime = models.DateTimeField(validators=[validate_start_datetime])


class Target(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    email = models.EmailField()
    sending_datetime = models.DateTimeField(null=True)

    def update_sending_datetime(self):
        self.sending_datetime += timedelta(weeks=self.customer.interval)
        self.save()


class TargetLog(models.Model):
    target = models.ForeignKey(Target, on_delete=models.CASCADE)
    email = models.EmailField()
    scheduled_datetime = models.DateTimeField()
    sending_datetime = models.DateTimeField(auto_now_add=True, blank=True)
    send = models.BooleanField()
    error_message = models.TextField(null=True, blank=True)

    @staticmethod
    def add_log(target, send, error_message):
        TargetLog.objects.create(
            target=target,
            email=target.email,
            scheduled_datetime=target.sending_datetime,
            sending_datetime=timezone.now(),
            send=send,
            error_message=error_message
        )


def generate_datetimes_for_targets(sender, instance, **kwargs):
    # Returns list of spreads evenly intervals excluding weekend
    targets = instance.target_set.all()
    if not targets:
        return
    
    MINUTES_IN_WORK_WEEK = 60*24*5
    target_interval = instance.interval*MINUTES_IN_WORK_WEEK/len(targets)
    start_dt = instance.start_datetime

    for i, t in enumerate(targets):
        dt = start_dt + timedelta(minutes=i*target_interval)
        if dt.weekday() >= 5:
            days_to_work_day = 7-dt.weekday()
            start_dt += timedelta(days=days_to_work_day)
        t.sending_datetime = start_dt + timedelta(minutes=i*target_interval)
        t.save()

post_save.connect(generate_datetimes_for_targets, sender=Customer)
