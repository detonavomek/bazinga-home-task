from django.core.management.base import BaseCommand, CommandError

from scheduler.tests.factories import CustomerFactory, TargetFactory, TargetLogFactory

class Command(BaseCommand):
    help = 'Generate fake data'

    def handle(self, *args, **options):
        c_a = [CustomerFactory(name='test3') for _ in range(5)]
        for c in c_a:
            c.save()
            t_a = [TargetFactory(customer=c) for _ in range(80)]
            for t in t_a:
                t.save()
                [TargetLogFactory(target=t).save() for _ in range(3)]
            c.save()
            
        self.stdout.write(self.style.SUCCESS('Data loaded successfully'))
