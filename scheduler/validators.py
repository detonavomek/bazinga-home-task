from datetime import timedelta

from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

def validate_start_datetime(value):
    if value < timezone.now() - timedelta(minutes=1):
        raise ValidationError(
            _('Datetime should be in the future'),
        )
