# Generated by Django 3.0 on 2020-07-15 15:27

from django.db import migrations, models
import django.db.models.deletion
import scheduler.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('interval', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4)], default=1)),
                ('start_datetime', models.DateTimeField(validators=[scheduler.validators.validate_start_datetime])),
            ],
        ),
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('sending_datetime', models.DateTimeField(null=True)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='scheduler.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='TargetLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('scheduled_datetime', models.DateTimeField()),
                ('sending_datetime', models.DateTimeField(auto_now_add=True)),
                ('send', models.BooleanField()),
                ('error_message', models.TextField(blank=True, null=True)),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='scheduler.Target')),
            ],
        ),
    ]
