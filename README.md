# Bazinga Home Task

# Install

1. You need to install and run docker: https://docs.docker.com/docker-for-mac/install/
2. Go to the suitable directory in the console
3. Clone project `git clone git@gitlab.com:detonavomek/bazinga-home-task.git`
4. Go to main project directory `cd bazinga`
5. Install project `make init`
6. Run project `make run`
7. Go to admin page: http://127.0.0.1:8000/admin/

# Helpers

All helpers are inside `Makefile`
1. Upload data `make loaddata`
2. Create superuser `make createsuperuser`
3. If you need to run commands directly in python container: `docker-compose run web <command>`

# General information

Customers information are in the `Customers` model. 
Each target stored in `Target` models.
Table `TargetLog` is for storing all logs. We can move it to NoSQL logs system.

Field `sending_datetime` is datatime information when to send next email. Update it by calling `update_sending_datetime` method inside `tasks.send_target_mails`.

Calculating time logic is inside `models.generate_datetimes_for_targets` signal. For now logic of calling this function is next: store customer information; store all targets information with empty `sending_datetime`; store customer information again. During each saving customer information we: get all its targets; calculate the time interval between sending emails to have a similar interval between them; set new `sending_datetime` for each customer's target based on interval. Now there is `save` for each customer separately as we have only 10 targets(based on home task), but as first performance improvement, we can use bulk save.


# Used technologies

Python, Django, Docker, docker compose, Celery, Celery-beat, Redis, PostgreSQL, factory-boy.

# Bonus questions

1. When should this scheduler run? On every day? Hour? Minute?
We should run scheduler at the event. If customer change data/targets/etc. On the event of adding new customer with targets, we could calculate sending time for each target. Then save the required data and move information about emails and times to another service. The final gate will know the time when it should send the next email and call the action at that time.

2. How should we handle interval settings change? For example, the customer decided to change the interval from 2 to 3 or vice versa
Depends on business logic. Quick thoughts: we can get all targets and recalculate time for them to distribute by the next interval. "Set time to targets as if it's a new customer".

3. What possible failures can happen? How should we handle them?
Firstly, it's time issues. Some countries change the time for 1 hour twice a week. In this case, we need to think about the logic of normal distribution here. Also could be problems if the company have targets in different timezone: in one timezone it could be already a weekend but in another still working day(and on the contrary).
Secondary, it's performance issues. If we should send a lot of emails at the same time, we could get a big queue and it takes more time to finish a task.
Next issue could be with tracking history. When a customer changes an interval, we recalculate sending time and don't store previous time.

# Possible improvements not included in the current solution

1. Tests. I can add them if needed. Did not include them because it takes time to design and write good tests. And this is a home task without future using.
2. Deploy configs and CI pipelines. Same reason as above.
3. Letters scheduled for 24 hours during workdays, not only working hours.
4. Fake email sending(random return 10% fails).
5. Editing Customers and Target. A lot of open questions and business logic with specific field validation.
6. Import/export logic.
7. Not save customer information two times(before adding targets and after). When we implement "add a new target to existing customer" functionality, there should be some business logic about how to recalculate time(bonus question 2). After implementing this logic we avoid saving customer information the second time.
8. Use bulk save instead of saving one bu one in `generate_datetimes_for_targets` method.
